//
//  ViewController.m
//  FoodAPIApp
//
//  Created by Kluster on 2016-05-25.
//  Copyright © 2016 Oleg Lindvin. All rights reserved.
//

#import "ViewController.h"
#import "TableViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *inputField;
@property (nonatomic) NSMutableArray *foods;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)search:(id)sender {
    
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff?query=%@", self.inputField.text];
    NSString *escaped = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:escaped];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        if(error) {
            NSLog(@"Error: %@", error);
            return;
        }
        NSError *jsonParseError = nil;
        NSMutableArray *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        if(jsonParseError) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if(result.count<=0) {
            } else {
                self.foods = result;
                //NSLog(@"Result: %@", self.foods);
            }
        });
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"searchResult" sender:self];
        });
    }];
    
    [task resume];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    TableViewController *tableView = [segue destinationViewController];
    tableView.listOfFood = self.foods;
}

@end
