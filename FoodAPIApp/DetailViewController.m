//
//  DetailViewController.m
//  FoodAPIApp
//
//  Created by Kluster on 2016-05-25.
//  Copyright © 2016 Oleg Lindvin. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *kcal;
@property (weak, nonatomic) IBOutlet UILabel *fat;
@property (weak, nonatomic) IBOutlet UILabel *protein;
@property (weak, nonatomic) IBOutlet UILabel *cabrs;
@property (weak, nonatomic) IBOutlet UILabel *cholesterol;
@property (weak, nonatomic) IBOutlet UILabel *vitC;
@property (weak, nonatomic) IBOutlet UILabel *iron;
@property (weak, nonatomic) IBOutlet UILabel *goodness;

@property (nonatomic) NSMutableDictionary *foods;


@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.foods = [[NSMutableDictionary alloc] init].mutableCopy;
    
    [self getGoodness];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getGoodness {
    NSString *s = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", self.number];
    
    NSString *escaped = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSURL *url = [NSURL URLWithString:escaped];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError * error) {
        
        if(error) {
            NSLog(@"Error: %@", error);
            return;
        }
        
        NSError *jsonParseError = nil;
        
        NSMutableDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonParseError];
        
        if(jsonParseError) {
            NSLog(@"Failed to parse data: %@", jsonParseError);
            return;
        }
        
        self.foods = [result objectForKey:@"nutrientValues"];
        
        self.title = [result objectForKey:@"name"];
        
        NSLog(@"Result: %@", self.foods);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(result.count<=0) {
            } else {
                self.protein.text = [NSString stringWithFormat:@"%@", [self.foods objectForKey:@"protein"]];
                self.fat.text = [NSString stringWithFormat:@"%@", [self.foods objectForKey:@"fat"]];
                self.kcal.text = [NSString stringWithFormat:@"%@", [self.foods objectForKey:@"energyKcal"]];
                self.cabrs.text = [NSString stringWithFormat:@"%@", [self.foods objectForKey:@"carbohydrates"]];
                self.vitC.text = [NSString stringWithFormat:@"%@", [self.foods objectForKey:@"vitaminC"]];
                self.cholesterol.text = [NSString stringWithFormat:@"%@", [self.foods objectForKey:@"cholesterol"]];
                self.iron.text = [NSString stringWithFormat:@"%@", [self.foods objectForKey:@"iron"]];
                
                int fat = [[self.foods objectForKey:@"fat"] intValue];
                int protein = [[self.foods objectForKey:@"protein"] intValue];
                int goodValue = fat * protein;
                self.goodness.text = [NSString stringWithFormat:@"%d", goodValue];
            }
            
        });
    }];
    
    [task resume];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
