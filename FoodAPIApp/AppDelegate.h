//
//  AppDelegate.h
//  FoodAPIApp
//
//  Created by Kluster on 2016-05-25.
//  Copyright © 2016 Oleg Lindvin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

