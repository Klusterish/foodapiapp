//
//  DetailViewController.h
//  FoodAPIApp
//
//  Created by Kluster on 2016-05-25.
//  Copyright © 2016 Oleg Lindvin. All rights reserved.
//

#import "ViewController.h"

@interface DetailViewController : ViewController
@property (nonatomic) NSNumber *number;

@end
