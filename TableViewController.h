//
//  TableViewController.h
//  FoodAPIApp
//
//  Created by Kluster on 2016-05-25.
//  Copyright © 2016 Oleg Lindvin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController

@property (nonatomic) NSMutableArray *listOfFood;

@end
